## Accesos oculares

Using eye tracking, we want to correlate the eye tracking information with the representational system which is beong used by the user. 
More information? send a line please, to: artyficialsas at gmail.com

## Browser Support

The following browsers support WebGazer.js:

* Google Chrome
* Microsoft Edge
* Mozilla Firefox
* Opera

Your browser needs to support the getUserMedia API as seen [here](http://caniuse.com/#feat=stream).

##How to run

- Clone/download
- Run eyes.html

## Citation
This project is based on the amazing # [WebGazer.js](https://webgazer.cs.brown.edu) plugin. A great work by: 
	@inproceedings{papoutsaki2016webgazer,
	author     = {Alexandra Papoutsaki and Patsorn Sangkloy and James Laskey and Nediyana Daskalova and Jeff Huang and James Hays},
	title      = {{WebGazer}: Scalable Webcam Eye Tracking Using User Interactions},
    booktitle  = {Proceedings of the 25th International Joint Conference on Artificial Intelligence (IJCAI-16)},
    pages      = {3839--3845},
	year       = {2016},
	organization={AAAI}
	}


## License

Copyright (C) 2016 [Brown HCI Group](http://hci.cs.brown.edu)

Licensed under GPLv3. Companies with a valuation of less than $10M can use WebGazer.js under LGPLv3.
